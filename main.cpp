#include <activemq/core/ActiveMQConnectionFactory.h>
#include <activemq/library/ActiveMQCPP.h>
#include <atomic>
#include <chrono>
#include <cstdlib>
#include <filesystem>
#include <string>
#include <thread>

struct ActiveMQInitializer {
  ActiveMQInitializer() { activemq::library::ActiveMQCPP::initializeLibrary(); }
  ~ActiveMQInitializer() { activemq::library::ActiveMQCPP::shutdownLibrary(); }
};

struct TimeoutMonitor {
  void operator()() {
    auto end = std::chrono::steady_clock::now() + std::chrono::seconds(3);
    do {
      std::this_thread::yield();
    } while (!done && std::chrono::steady_clock::now() < end);

    if (!done) {
      exit(EXIT_FAILURE);
    }
  }

  std::atomic_bool done = false;
};

int main(int argc, char *argv[]) {
  std::string broker_uri;

  if (argc > 1) {
    broker_uri = argv[1];
  } else {
    const char *jms_url = std::getenv("EAMIS_JMS_URL");
    if (jms_url) {
      broker_uri.assign(jms_url);
    }
  }

  if (broker_uri.empty()) {
    std::cerr << "Broker URI not provided!\n";
    return EXIT_FAILURE;
  }

  ActiveMQInitializer activemq_initializer;

  int exit_code = EXIT_SUCCESS;
  TimeoutMonitor timeout_monitor;
  std::thread timeout_monitor_thread(std::ref(timeout_monitor));

  try {
    auto connection_factory = std::make_unique<activemq::core::ActiveMQConnectionFactory>(broker_uri);

    connection_factory->setClientId(std::filesystem::path(argv[0]).filename());

    std::unique_ptr<cms::Connection> connection(connection_factory->createConnection());

    connection->start();

    timeout_monitor.done = true;

    connection->stop();
    connection->close();
  } catch (const std::exception &e) {
    timeout_monitor.done = true;
    exit_code = EXIT_FAILURE;
  }

  timeout_monitor_thread.join();

  return exit_code;
}
